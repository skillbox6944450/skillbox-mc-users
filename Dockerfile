FROM openjdk:17

# Environmet variables
ENV SERVICE_HOME=/opt/service
ENV JAR_FILE=${SERVICE_HOME}/ms-users-0.0.1-SNAPSHOT.jar

# Copy builded jar
ADD build/libs $SERVICE_HOME

# Run service
ENTRYPOINT java -jar ${JAR_FILE}