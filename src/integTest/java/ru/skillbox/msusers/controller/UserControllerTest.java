package ru.skillbox.msusers.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.skillbox.msusers.container.PostgresContainerWrapper;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserControllerTest {
    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper>
            postgresContainer = new PostgresContainerWrapper();
    @Autowired
    private MockMvc mockMvc;

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Test
    @Order(1)
    void testCorrectCreateUser() throws Exception {
        String request = "{\"name\": \"Kirill\", \"email\": \"kzhelnov19@yandex.ru\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().string(containsString("1")));
    }

    @Test
    @Order(2)
    void testCorrectCreateUser2() throws Exception {
        String request = "{\"name\": \"Kirill 2\", \"email\": \"kzhelnov2@yandex.ru\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().string(containsString("2")));
    }

    @Test
    @Order(3)
    void testGetUserById() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/1")
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.equalTo("Kirill")))
                .andExpect(MockMvcResultMatchers.
                        jsonPath("$.email", Matchers.equalTo("kzhelnov19@yandex.ru")));
    }

    @Test
    @Order(4)
    void testCreateSubscribe() throws Exception {
        mockMvc.perform(post("/users/1/subscriptions/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(5)
    void whenGetAllUsers_thenReturnUsersJson() throws Exception {

        mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].name", is("Kirill")))
                .andExpect(jsonPath("$[1].name", is("Kirill 2")))
                .andExpect(jsonPath("$[0].email", is("kzhelnov19@yandex.ru")))
                .andExpect(jsonPath("$[1].email", is("kzhelnov2@yandex.ru")));
    }

    @Test
    @Order(6)
    void whenGetSubscribers_thenReturnUsersJson() throws Exception {
        mockMvc.perform(get("/users/2/subscribers")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Kirill")))
                .andExpect(jsonPath("$[0].email", is("kzhelnov19@yandex.ru")));
    }

    @Test
    @Order(7)
    void whenGetSubscriptions_thenReturnUsersJson() throws Exception {
        mockMvc.perform(get("/users/1/subscriptions")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].name", is("Kirill 2")))
                .andExpect(jsonPath("$[0].email", is("kzhelnov2@yandex.ru")));
    }

    @Test
    @Order(8)
    void whenDeleteUserById_thenReturnUserJson() throws Exception {
        mockMvc.perform(delete("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1")));
    }

    @Test
    @Order(9)
    void whenGetAllUsersAfterDelete_thenReturnUsersJson() throws Exception {

        mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].name", is("Kirill 2")))
                .andExpect(jsonPath("$[0].email", is("kzhelnov2@yandex.ru")));
    }

}