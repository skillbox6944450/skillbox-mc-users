package ru.skillbox.msusers.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.msusers.entity.User;
import ru.skillbox.msusers.exception.IncorrectUserIdException;
import ru.skillbox.msusers.repo.UserRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;
    @Mock
    UserRepository userRepository;

    @Mock
    User requestUser;
    @Mock
    User savedUser;
    @Mock
    List<User> users;

    @Test
    @DisplayName("Если пользователь создан в базе, то возвращается его id.")
    void whenUserCreateSuccess_thenReturnUserId() {
        when(userRepository.save(requestUser)).thenReturn(savedUser);
        when(savedUser.getId()).thenReturn(1L);

        long userId = userService.createUser(requestUser);

        assertEquals(userId, 1L, "Возвращен некорректный id");
    }

    @Test
    @DisplayName("Если пользователь создан в базе, то возвращается его id.")
    void whenUserCreateFailed_thenThrowException() {
        when(userRepository.save(requestUser)).thenThrow(new RuntimeException("Ошибка создания."));

        assertThrows(RuntimeException.class, () -> userService.createUser(requestUser));
    }

    @Test
    @DisplayName("Если запрос на получение пользователей успешен, то возвращается их массив")
    void correctGetAllUsers() {
        when(userRepository.findAll()).thenReturn(users);
        assertEquals(userService.getAllUsers(), users, "Не был возвращен список пользователей.");
    }

    @Test
    @DisplayName("Если пользователь удален, то возвращается id удаленного пользователя.")
    void whenDeleteUserSuccess_thenReturnDeletedIdUser() {
        when(userRepository.existsById(1L)).thenReturn(true);
        doNothing().when(userRepository).deleteById(1L);

        userService.deleteUser(1L);

        verify(userRepository).deleteById(eq(1L));
    }

    @Test
    @DisplayName("Если при удалении пользователь не найден, тогда выкидывается ошибка")
    void whenDeleteUserFailed_thenReturnException() {
        when(userRepository.existsById(1L)).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> userService.deleteUser(1L),
                "Не было выброшено исключение.");
    }

    @Test
    @DisplayName("Корректно выводятся все подписки")
    void correctGetSubscriptions() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(savedUser));
        when(savedUser.getSubscriptions()).thenReturn(users);

        userService.getSubscriptions(1L);

        assertEquals(users, userService.getSubscriptions(1L), "Был возвращен некорректный список.");
    }

    @Test
    @DisplayName("Коррект выводятся подписанты")
    void correctGetSubscribers() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(savedUser));
        when(savedUser.getSubscribers()).thenReturn(users);

        userService.getSubscribers(1L);

        assertEquals(users, userService.getSubscribers(1L), "Был возвращен некорректный список");
    }

    @Test
    @DisplayName("Корректно отрабатывает запрос получения пользователия по id")
    void whenGetUserSuccess_thenReturnObjectUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(savedUser));

        assertEquals(savedUser, userService.getUser(1L), "Не был возвращен объект пользователя");
    }

    @Test
    @DisplayName("Выкидывается исключение если пользователь не был найден")
    void whenGetUserFailed_thenThrowException() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        Executable executable = () -> userService.getUser(1L);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class,
                executable, "Не был возвращен объект пользователя");
        assertEquals("404 NOT_FOUND \"Пользователь не найден.\"", responseStatusException.getMessage());
    }

    @Test
    @DisplayName("Если пользователь существует, то проходит запрос на обновление данных")
    void whenUpdateUserIsExists_thenCorrectUpdate() {
        when(userRepository.existsById(1L)).thenReturn(true);
        when(userRepository.save(requestUser)).thenReturn(savedUser);

        assertEquals(savedUser, userService.updateUser(requestUser, 1L),
                "Обновлен некорректный объект.");
    }

    @Test
    @DisplayName("Выкидывается исключение, если пользователь не найден при обновлении")
    void whenUpdateUserIsExistsFalse_thenThrowException() {
        when(userRepository.existsById(1L)).thenReturn(false);

        assertThrows(ResponseStatusException.class,
                () -> userService.updateUser(requestUser, 1L),
                "Не было выброшено исключение.");
    }

    @Test
    @DisplayName("Если переданные айди корректы, то происходит подписка")
    void whenSubscribeIdIsCorrect_thenSubscribe() {
        User subscriber = mock(User.class);
        User user = mock(User.class);
        when(userRepository.findById(1L)).thenReturn(Optional.of(subscriber));
        when(userRepository.findById(2L)).thenReturn(Optional.of(user));

        userService.subscribe(1L, 2L);

        verify(subscriber).subscribe(user);
        verify(userRepository).save(subscriber);
    }

    @Test
    @DisplayName("Если айди некорректные, то при подписке выкидывается исключение")
    void whenSubscribeIdIncorrect_thenSubscribe() {
        assertThrows(IncorrectUserIdException.class,
                () -> userService.subscribe(1L, 1L));
    }

    @Test
    @DisplayName("Если переданные айди корректны, то происходит отписка")
    void whenUnsubscribeIdIsCorrect_thenSubscribe() {
        User subscriber = mock(User.class);
        User user = mock(User.class);
        when(userRepository.findById(1L)).thenReturn(Optional.of(subscriber));
        when(userRepository.findById(2L)).thenReturn(Optional.of(user));

        userService.unsubscribe(1L, 2L);

        verify(subscriber).unsubscribe(user);
        verify(userRepository).save(subscriber);
    }

    @Test
    @DisplayName("Если айди некорректные, то при отписке выкидывается исключение")
    void whenUnsubscribeIdIncorrect_thenSubscribe() {
        assertThrows(IncorrectUserIdException.class,
                () -> userService.unsubscribe(1L, 1L));
    }

}