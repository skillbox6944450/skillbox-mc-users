package ru.skillbox.msusers.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.msusers.entity.User;
import ru.skillbox.msusers.service.UserService;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    @Mock
    User user;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    private static final String NAME = "Kirill";
    private static final String NAME_2 = "Kirill2";
    private static final String EMAIL = "test@test.ru";
    private static final String EMAIL_2 = "test2@test.ru";


    @BeforeEach
    void init() {
        lenient().when(user.getId()).thenReturn(1L, 2L);
        lenient().when(user.getName()).thenReturn(NAME, NAME_2);
        lenient().when(user.getEmail()).thenReturn(EMAIL, EMAIL_2);
    }

    @Test
    void whenGetUserById_thenReturnUserJson() throws Exception {

        when(userService.getUser(1L)).thenReturn(user);

        mvc.perform(get("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.email", is(EMAIL)));
    }

    @Test
    void whenDeleteUserById_thenReturnUserJson() throws Exception {

        when(userService.deleteUser(1L)).thenReturn(1L);

        mvc.perform(delete("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1")));
    }

    @Test
    void subscribeUser() throws Exception {
        doNothing().when(userService).subscribe(1L, 2L);

        mvc.perform(post("/users/1/subscriptions/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(userService).subscribe(1L, 2L);
    }

    @Test
    void unsubscribeUser() throws Exception {
        doNothing().when(userService).unsubscribe(1L, 2L);

        mvc.perform(delete("/users/1/subscriptions/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(userService).unsubscribe(1L, 2L);
    }

    @Test
    void whenGetAllUsers_thenReturnUsersJson() throws Exception {

        when(userService.getAllUsers()).thenReturn(Arrays.asList(user, user));

        mvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[1].name", is(NAME_2)))
                .andExpect(jsonPath("$[0].email", is(EMAIL)))
                .andExpect(jsonPath("$[1].email", is(EMAIL_2)));
    }


    @Test
    void whenCreateUser_thenReturnUserId() throws Exception {
        User user = new User();
        user.setEmail(EMAIL);
        user.setName(NAME);

        when(userService.createUser(any())).thenReturn(1L);

        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(user))
                )
                .andExpect(status().isOk()).andExpect(content().string(containsString("1")));

        verify(userService).createUser(userArgumentCaptor.capture());
        assertEquals(NAME, userArgumentCaptor.getValue().getName());
        assertEquals(EMAIL, userArgumentCaptor.getValue().getEmail());

    }

    @Test
    void whenUpdateUser_thenReturnUserId() throws Exception {
        User user = new User();
        user.setId(2L);
        user.setEmail(EMAIL_2);
        user.setName(NAME_2);

        when(userService.updateUser(any(), eq(2L))).thenReturn(user);

        mvc.perform(put("/users/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(user))
                )
                .andExpect(status().isOk()).andExpect(content().string(containsString("2")));
    }

    @Test
    void whenGetSubscribers_thenReturnUsersJson() throws Exception {

        when(userService.getSubscribers(3L)).thenReturn(Arrays.asList(user, user));

        mvc.perform(get("/users/3/subscribers")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[1].name", is(NAME_2)))
                .andExpect(jsonPath("$[0].email", is(EMAIL)))
                .andExpect(jsonPath("$[1].email", is(EMAIL_2)));
    }

    @Test
    void whenGetSubscriptions_thenReturnUsersJson() throws Exception {

        when(userService.getSubscriptions(3L)).thenReturn(Arrays.asList(user, user));

        mvc.perform(get("/users/3/subscriptions")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[1].name", is(NAME_2)))
                .andExpect(jsonPath("$[0].email", is(EMAIL)))
                .andExpect(jsonPath("$[1].email", is(EMAIL_2)));
    }
}