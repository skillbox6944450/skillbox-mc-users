create schema users_schema;
alter schema users_schema owner to postgres;

create table users_schema.users
(
    id    serial
        constraint "users_pk"
            primary key,
    name  varchar(30) not null,
    email varchar(30) not null,
    is_active boolean not null default true,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

alter table users_schema.users
    owner to postgres;

create table users_schema.subscriptions
(
    subscriber_id integer not null
        constraint subscriptions_users_id_fk
            references users_schema.users,
    user_id       integer not null
        constraint subscriptions_users_id_fk2
            references users_schema.users
);

alter table users_schema.subscriptions
    owner to postgres;

