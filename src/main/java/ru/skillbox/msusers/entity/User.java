package ru.skillbox.msusers.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@SQLRestriction("is_active = true")
@SQLDelete(sql = "UPDATE users_schema.users SET is_active = false where id=?")
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "is_active")
    @JsonIgnore
    private Boolean isActive = Boolean.TRUE;

    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime updatedAt;

    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinTable(name = "subscriptions",
    joinColumns = @JoinColumn(name = "subscriber_id"),
    inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> subscriptions;

    @ManyToMany(mappedBy = "subscriptions", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<User> subscribers;

    public void subscribe(User user) {
        this.subscriptions.add(user);
        user.subscribers.add(this);
    }

    public void unsubscribe(User user) {
        this.subscriptions.remove(user);
        user.subscribers.remove(user);
    }
}
