package ru.skillbox.msusers.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.msusers.entity.User;
import ru.skillbox.msusers.exception.IncorrectUserIdException;
import ru.skillbox.msusers.repo.UserRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public long createUser(User user) {
        User savedUser = userRepository.save(user);
        return savedUser.getId();
    }

    @Override
    public User getUser(long id) {
        return getUserOrThrow(id);
    }

    @Override
    public User updateUser(User user, long id) {
        if (userRepository.existsById(id)) {
            return userRepository.save(user);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден.");
        }
    }

    @Override
    public long deleteUser(Long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден.");
        }
        userRepository.deleteById(id);

        return id;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void subscribe(Long subscriberId, Long userId) {
        if (subscriberId.equals(userId)) {
            throw new IncorrectUserIdException();
        }
        User subscriber = getUserOrThrow(subscriberId);
        User user = getUserOrThrow(userId);
        subscriber.subscribe(user);

        userRepository.save(subscriber);
    }

    @Override
    public void unsubscribe(Long subscriberId, Long userId) {
        if (subscriberId.equals(userId)) {
            throw new IncorrectUserIdException();
        }
        User subscriber = getUserOrThrow(subscriberId);
        User user = getUserOrThrow(userId);
        subscriber.unsubscribe(user);

        userRepository.save(subscriber);
    }

    @Override
    public List<User> getSubscriptions(Long userId) {
        return getUserOrThrow(userId).getSubscriptions();
    }

    @Override
    public List<User> getSubscribers(Long userId) {
        return getUserOrThrow(userId).getSubscribers();
    }

    private User getUserOrThrow(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден."));
    }
}

