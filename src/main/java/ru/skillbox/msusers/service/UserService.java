package ru.skillbox.msusers.service;


import ru.skillbox.msusers.entity.User;

import java.util.List;

public interface UserService {
    long createUser(User user);

    User getUser(long id);

    User updateUser(User user, long id);

    long deleteUser(Long id);

    List<User> getAllUsers();

    void subscribe(Long subscriberId, Long userId);

    List<User> getSubscriptions(Long userId);

    List<User> getSubscribers(Long userId);

    void unsubscribe(Long subscriberId, Long userId);
}
