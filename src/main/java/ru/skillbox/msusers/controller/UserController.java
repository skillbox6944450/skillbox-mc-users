package ru.skillbox.msusers.controller;

import ru.skillbox.msusers.entity.User;
import ru.skillbox.msusers.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping
    public long createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PutMapping("/{id}")
    public long updateUser(@RequestBody User user, @PathVariable Long id) {
        return userService.updateUser(user, id).getId();
    }

    @DeleteMapping("/{id}")
    public long deleteUser(@PathVariable Long id) {
        return userService.deleteUser(id);
    }

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/{subscriberId}/subscriptions/{userId}")
    public void subscribe(@PathVariable Long subscriberId, @PathVariable Long userId) {
        userService.subscribe(subscriberId, userId);
    }

    @DeleteMapping("/{subscriberId}/subscriptions/{userId}")
    public void unsubscribe(@PathVariable Long subscriberId, @PathVariable Long userId) {
        userService.unsubscribe(subscriberId, userId);
    }


    @GetMapping("/{userId}/subscriptions")
    public List<User> getSubscriptions(@PathVariable Long userId) {
        return userService.getSubscriptions(userId);
    }

    @GetMapping("/{userId}/subscribers")
    public List<User> getSubscribers(@PathVariable Long userId) {
        return userService.getSubscribers(userId);
    }

}
